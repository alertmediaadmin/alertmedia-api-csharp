# README #

The api definition comes from swagger.yaml

## To build locally

#### Prerequisites
Follow instructions at https://github.com/swagger-api/swagger-codegen.

You'll need:
 * JRE 7+
 * MVN
 * The usual JAVA_HOME and PATH stuff
 * swagger-codegen-cli (`wget http://central.maven.org/maven2/io/swagger/swagger-codegen-cli/2.2.3/swagger-codegen-cli-2.2.3.jar -O swagger-codegen-cli.jar`)

#### Generate API classes
In powershell run `./codegen.ps1`

Now you can open the solution and run the example program.

Anytime you make a change to the swagger.yaml, you should run `./codegen.ps1 -clean` and never commit any of the artifacts (they should be .gitignored).
