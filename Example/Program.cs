﻿using System;
using System.IO;
using AlertMedia.Client;

namespace AlertMedia.Example
{
    class Program
    {
        private static AlertMediaClient _client;

        static void Main(string[] args) {
 
            _client = new AlertMediaClient(
                clientId:"00000000-0000-0000-0000-000000000000", 
                clientSecretKey:"00000000000000000000"
            );
            
            /* 
             * Uncomment any the following examples to try out the client
             */
            #region Examples
            // ListUsers();
            //User _user = new User();
            //_user.FirstName = "Jeff";
            //_user.LastName = "Test";
            //_user.Email = "jeff.test1@alertmedia.com";
            //User _newUser = _client.Users.Create(_user);
            //_newUser.FirstName = "NewJeff";
            //_client.Users.Update(_newUser.Id, _newUser);

            //Group _newGroup = new Group();
            //_newGroup.Name = "MyGroup";
            //_newGroup = _client.Groups.Create(_newGroup);
            //_newGroup.Name = "MyNewGroup";
            //_client.Groups.Update(_newGroup.Id, _newGroup);

            //userid 3727556
            //groupid 90880

            _client.Groups.AddUsers(90880, new Collection<long?>() { 3727556, 3727557 });

            //_client.Groups.RemoveUsers(_newGroup.Id, new Collection<long?>() { _newUser.Id });

            //_client.Users.Delete(_newUser.Id);

            //_client.Users.Undelete(_newUser.Id);

            // SetApiUserPhoto("sample.png");
            #endregion

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        static void ListUsers() {
            foreach (var user in _client.Users.List(_client.ApiCustomer.Id))
                Console.WriteLine($"{user.Id}: {user.FirstName} {user.LastName}");
        }

        static void SetApiUserPhoto(string path) {
            using (var stream = new FileStream(path, FileMode.Open))
                _client.Users.SetPhoto(_client.ApiUser.Id, stream);
        }

    }
}
