<#
.EXAMPLES
>>> ./codegen.ps1 -cli swagger-codegen-cli.jar -output "./CodeGen"
#>
[CmdletBinding()]
Param ( 
    [string] $CLI="swagger-codegen-cli.jar",
    [string] $Target="AlertMedia.CodeGen",
    [string] $BuildPath="${PSScriptRoot}/CodeGenBuild",
    [hashtable] $Options=@{
        packageName="$Target"
        packageGuid="AF177862-3F69-4137-B504-06E4D40B48D8"
        sourceFolder="src"
        useCollection="true"
        returnICollection="true"
        #netCoreProjectFile="true"
        targetFramework="v4.5"
    },
    [string] $Lang="csharp",
    [Switch] $Clean,
    [Switch] $Help
)
$ErrorActionPreference = "Stop"

if ($Help) {
    java -jar "$CLI" config-help -l "${Lang}"
    Exit(0)
} 

if ($Clean){
    Remove-Item $Target -Recurse -ErrorAction Ignore
}

$options_arg = ( $options.Keys | %{ $_ + "=" + $options[$_] } ) -join ","

java -jar "${CLI}" generate `
    --input-spec "swagger.yaml" `
    --lang "${Lang}" `
    --output "${BuildPath}" `
    --template-dir "swagger-templates/${Lang}" `
    -D $options_arg

$curr_location = Get-Location
try {
    Write-Verbose "Removing ${PSScriptRoot}/$($Options['packageName'])"
    Remove-Item "${PSScriptRoot}/$($Options['packageName'])" -Force -Recurse -ErrorAction Ignore -Verbose:$verbose
    Set-Location "${BuildPath}/$($Options['sourceFolder'])"
    Move-Item $Options['packageName'] ../../ -Force -Verbose:$verbose
} finally {
    Set-Location $curr_location
    Remove-Item "${BuildPath}" -Recurse -ErrorAction Ignore
}

Exit(0)
